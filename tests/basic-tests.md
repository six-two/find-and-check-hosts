# Tests

Invalid IP: 1.1.1.256, 1.1.1, 1.1.1.1.1
List: 1.1.1.1, 1.1.1.2
In URL: http://1.1.1.1/test?a=b https://user:password@example.com/page#L10
In Markdown code, [1.1.1.1](https://example.com:443), `1.1.1.1`, **1.1.1.1**, _1.1.1.1_, ~1.1.1.1~
Paths: cat ~/.config/i3/config > /tmp/whatever.txt ; cat .bashrc test.txt ~/tools/eicar.com
Inofficial TLDs: example.lan, example.intern, example.corp
1.1.1.1:2.2.2.2 sed s/1.1.1.1/2.2.2.2/g
ip=1.1.1.1
eXampLe.coM
Can can use the value `WScript.Shell` to ... (should not match WScript.Shells)
